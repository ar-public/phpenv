# Инструкция по установке и настройке PHP окружения altrecipe

## 1. Установить докер и докер-компоуз
### 1.1. Для мака:

[Инструкция](https://docs.docker.com/docker-for-mac/install/)

После установки докера на мак нужно поставить еще docker-sync:
```
gem install docker-sync
```

### 1.2. Для ubuntu (используем ubuntu 18.04):

сначала ставим сам докер проходимся по пункту 1(и 2 по желанию):

[Инструкция](https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-18-04)

Потом ставим docker-compose(здесь выполняем только шаг 1):

[Инструкция](https://www.digitalocean.com/community/tutorials/how-to-install-docker-compose-on-ubuntu-18-04)

## 2. Запускаем скрипт разворачивания докера:

```
curl https://gitlab.com/ar-public/phpenv/raw/master/phpenv.sh -k --output phpenv.sh && chmod +x phpenv.sh && ./phpenv.sh -p
```



Скрипт создаст нужную структуру папок и установит необходимые докер имейджи.

После выполнения скрипта у тебя будет 2 папки - docker и project
в папке docker находятся конфиги php, nginx и файлы мускула. Если что - папку mysql можно бекапить и переносить на другой такой же докер.

В папке project разворачиваешь нужные тебе приложения и тд. По-умолчанию докер настроен смотреть в папку:

`project/web`

Чтобы это поменять вызываем команду

```
./phpenv.sh --conf-nginx
```

И в открывшемся редакторе меняем строку

`set $SITE_PUBLIC /web;`

путь прописывается относительно папки project. 
То есть если мы напишем 
```
set $SITE_PUBLIC /laravel/public;
```
То nginx будет смотреть в папку 

`project/laravel/public`

Чтобы nginx смотрел просто в папку project, то нужно поставить 
```
set $SITE_PUBLIC '';
```

После всех изменений нужно перезагрузить докер:
```
./phpenv.sh --reboot

```

Запустить имейджи можно командами:
<strong>Для сервера:</strong>
```
./phpenv.sh -s
```
<strong>На рабочей машине:</strong>
```
./phpenv.sh -sd
```

# 3. Документация по скрипту:

Вся документация по скрипту находится helpe самгого скрипта:
```
./phpenv.sh -h
```
Или запустить `./phpenv.sh` без параметров

Вот дубликат документации скрипта, для более удобного просмотра:
```
Usage: ./phpenv.sh [option...]

   -s, --run, --start		run containers
   -sd, --rund, --startd	run containers on development machine
   -p, --setup              	prepare folder structure and containers
   -d, --stop              	stop running containers
   -r, --reboot, --restart 	restart containers
   --clean              	clean containers cache
   --clean-run              	reboot containers with clean
   --clean-docker              	clean all docker stuff, but leave mysql DB and project
   --clean-full              	DANGER!!!! remove all folders, including database and your project
   --rebuild              	clean docker cache and rebuild
   --conf-nginx              	modify nginx.conf
   --conf-php              	modify php.ini
   --shell-php              	open comand line for PHP docker container
   --fix-dev              	fix access rules for dev project
   --shell-php-dev              open comand line for PHP-DEV docker container
   --shell-nginx             	open comand line for NGINX docker container
   --shell-mysql              	open comand line for MYSQL docker container
   --help, -h              	show this manual

```

# 4. Настройка xDebug на локальном докере

## 4.1 Узнаем IDE_KEY

Создаем в проекте файл info.php, пишем в нем phpinfo() и смотрим какой IDE key (используем поиск по страничке по ключу xdebug).

## 4.2 Идем в настройки IDE:

- В phpstorme идем `Languages & Frameworks > PHP > Debug`
- в пункте xdebug прописываем порт `13136` (или другой, который задавали в php.info)
- Разворачиваем пунктик Debug и переходим в DBGp proxy
- вводим IDE кей, который нашли в пункте 4.1 
- прописываем ардес сервера `localhost` и вводим порт `13136` (или другой, который задавали в php.info), 
нажимаем аплай и ок.

## 4.3 Настраиваем кофигурацию запуска:

- Добавляем Debug конфигурацию в пункте `Run > Edit configurations`
- Тыкаем галочку `Filter debug connection`
- Добавляем Сервер через 3 точки и настраиваем мапинг файлов файлы проекта мапим на `/var/www/site`
- Пишем сюда IDE кей, который нашли в пункте 4.1 и сохраняемся
- ставим в браузер какой-то плагин для xDebug (он должен отправлять определенные куки)






















