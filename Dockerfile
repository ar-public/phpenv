FROM php:7.4-fpm

RUN apt-get update && apt-get install -y \
	curl \
	wget \
	git \
	libfreetype6-dev \
	libjpeg62-turbo-dev \
	libxslt-dev \
	libicu-dev \
	libmcrypt-dev \
	libpng-dev \
	libxml2-dev \
	libpq-dev \
	libzip-dev \
	libonig-dev \
&& docker-php-ext-install -j$(nproc) iconv mysqli pdo_mysql zip \
&& docker-php-ext-configure gd --with-freetype --with-jpeg\
&& docker-php-ext-install -j$(nproc) gd \
&& pecl install xdebug-2.9.3 \
&& docker-php-ext-enable xdebug

RUN pecl install mcrypt-1.0.3
RUN docker-php-ext-enable mcrypt
RUN docker-php-ext-configure intl
RUN docker-php-ext-install intl
RUN docker-php-ext-install xsl
RUN docker-php-ext-install soap
RUN docker-php-ext-install pdo pdo_pgsql

RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

ADD php.ini /usr/local/etc/php/conf.d/40-custom.ini

WORKDIR /var/www/site

CMD ["php-fpm"]