# Пропишем версию
version: '3.3'
# Перечислим сервисы
services:
    __PREFIX__nginx:
        # Пропишем какой образ мы хотим использовать
        image: nginx:latest
        # Назовем свой контейнер по красивому
        container_name: __PREFIX__nginx
        # Проброс портов
        ports:
            - "80:80"
            - "443:443"
        # Проброс папок
        volumes:
            - ./docker/nginx/core:/etc/nginx/conf.d
            - ./project/:/var/www/site/
            - ./docker/nginx/Logs:/var/log/nginx/
            - ./docker/nginx/html:/usr/share/nginx/html/
        # Укажем зависимости
        links:
            - __PREFIX__php

    __PREFIX__db:

        image: postgres:10

        ports:
            - "54320:5432"

        container_name: __PREFIX__db
        environment:
            - POSTGRES_USER=__MYSQL_DBUSER__
            - POSTGRES_PASSWORD=__MYSQL_DBPASS__
            - POSTGRES_DB=__MYSQL_DBNAME__


        volumes:
            - ./docker/mysql:/var/lib/postgresql/


    __PREFIX__php:
        build: ./docker/php

        container_name: __PREFIX__php

        volumes:
            - ./project:/var/www/site

        links:
            - __PREFIX__db:db


volumes:
    db-data:
        driver: local
